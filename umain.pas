unit umain;

interface

uses
  uconst,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfrmMain = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure SetCaption(aCaption : WideString = '');
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  SetCaption();
end;

procedure TfrmMain.SetCaption(aCaption: WideString);
begin
  if aCaption = '' then Caption := FullName;
end;

end.
