program domain_info;

uses
  Forms,
  umain in 'umain.pas' {frmMain},
  uconst in 'uconst.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := AppName;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
